import java.util.*;

public class DataProvider {

    private List<City> cities;

    public DataProvider() {
        generateCities();
    }

    private List<Integer> generateRandomProcent(){
        List<Integer> values = new ArrayList<>();
        Random rand = new Random();

        for (int index = 0; index < 24; ++index)
            values.add(rand.nextInt(101));

        return values;
    }

    private List<Integer> generateRandomTemperatures() {
        List<Integer> temperatures = new ArrayList<>();
        Random rand = new Random();

        for (int index = 0; index < 24; ++index)
            temperatures.add(rand.nextInt(27) - 3);

        Collections.sort(temperatures.subList(0, temperatures.size() / 2));
        Collections.sort(temperatures.subList(temperatures.size() / 2, temperatures.size()));
        Collections.reverse(temperatures.subList(temperatures.size() / 2, temperatures.size()));
        return temperatures;
    }

    private void generateCities() {
        cities = new ArrayList<>();

        City brasov = new City("Brasov", 45.657974, 25.601198);
        brasov.setTemperatures(generateRandomTemperatures());
        brasov.setRainfall(generateRandomProcent());
        brasov.setHumidity(generateRandomProcent());
        City fagaras = new City("Fagaras", 45.8416, 24.9731);
        fagaras.setTemperatures(generateRandomTemperatures());
        fagaras.setRainfall(generateRandomProcent());
        fagaras.setHumidity(generateRandomProcent());
        City ghimbav = new City("Ghimbav", 45.6630, 25.5065);
        ghimbav.setTemperatures(generateRandomTemperatures());
        ghimbav.setRainfall(generateRandomProcent());
        ghimbav.setHumidity(generateRandomProcent());
        City codlea = new City("Codlea", 45.6977, 25.4561);
        codlea.setTemperatures(generateRandomTemperatures());
        codlea.setRainfall(generateRandomProcent());
        codlea.setHumidity(generateRandomProcent());
        City rupea = new City("Rupea", 46.0378, 25.2225);
        rupea.setTemperatures(generateRandomTemperatures());
        rupea.setRainfall(generateRandomProcent());
        rupea.setHumidity(generateRandomProcent());
        City rasnov = new City("Rasnov", 45.5746, 25.4540);
        rasnov.setTemperatures(generateRandomTemperatures());
        rasnov.setRainfall(generateRandomProcent());
        rasnov.setHumidity(generateRandomProcent());

        cities.add(brasov);
        cities.add(codlea);
        cities.add(fagaras);
        cities.add(ghimbav);
        cities.add(rasnov);
        cities.add(rupea);
    }

    public List<City> getCities() {
        return cities;
    }
}
