import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(asyncSupported = true)
public class WeatherServlet extends HttpServlet {

    DataProvider dataProvider = new DataProvider();
    String cityName = null, cityLatitude = null, cityLongitude = null;
    Date previousDate = null;
    int hour;
    static int changeHour = 0;

    // Method to handle GET method request.
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html");

        final AsyncContext acontext = request.startAsync();

        acontext.start(() -> {
            //response.setIntHeader("Refresh", 5);
            String citiesString = generateCitiesHtmlText();
            String formString = generateFormHtmlText();
            String clock = getHour();
            String result = "";

            String action = request.getParameter("action");
            if (!action.equals("Messages")) {
                cityName = acontext.getRequest().getParameter("cityName");
                cityLatitude = acontext.getRequest().getParameter("latitude");
                cityLongitude = acontext.getRequest().getParameter("longitude");
            }
            if (cityName != null || (cityLatitude != null && cityLongitude != null)) {
                result = generateSearchResultHtmlText(cityName, cityLatitude, cityLongitude);
            }

            String weatherHtml = "<html lang=\"en\">\n" +
                    "<head><meta charset=\"UTF-8\"></head>\n" +
                    "<body style=\"background-color: #9AD1D4; margin: 0px auto; text-align: center\">\n" + formString + "<br>\n" +
                    "<ul style=\"list-style: none;\">\n" + "<h2 style=\"color: #CC3363;\">Result:</h2>" +
                    "<b style=\"color: #CC3363;\">Current time:</b> " + clock + " update hour:" + hour + "<br>" +
                    "<b style=\"color: #CC3363;\">Search result:</b><br>" + result + "<br>" +
                    "<li><b style=\"color: #CC3363;\">City Name: </b> " + cityName + "\n" +
                    "<li><b style=\"color: #CC3363;\">Latitude: </b> " + cityLatitude + "\n" +
                    "<li><b style=\"color: #CC3363;\">Latitude: </b> " + cityLongitude + "\n" +
                    "</ul><br>\n" +
                    "<h2 style=\"color: #CC3363;\">All cities</h2>" + citiesString +
                    "</body>\n" +
                    "</html>";

            PrintWriter out = null;
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " +
                    "transitional//en\">\n";

            assert out != null;
            out.println(docType + weatherHtml);

            acontext.complete();
        });
    }

    // Method to handle POST method request.
    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        doGet(request, response);
    }

    private String generateCitiesHtmlText() {
        List<City> cities = dataProvider.getCities();
        return getHtmlTextForList(cities);
    }

    private String generateFormHtmlText() {

        return "<form action = \"WeatherServlet\" method = \"POST\">\n" +
                "<br><br><br><h1 style=\"color: #CC3363\">How is the weather today?</h1><br><br><br>" +
                "    <b style=\"color: #20063B\">City name:</b> <input type=\"text\" name=\"cityName\">\n" +
                "    <input style=\"background-color: #C2F9BB; color: #20063B\" type = \"submit\" name=\"action\" value = \"Search\" />\n" +
                "    <br><br>\n" +
                "    <b style=\"color: #20063B\">Latitude:</b> <input type=\"text\" name=\"latitude\" />\n" +
                "    <b style=\"color: #20063B\">Longitude:</b> <input type=\"text\" name=\"longitude\"/>\n" +
                "    <input style=\"background-color: #C2F9BB; color: #20063B\" type = \"submit\" name=\"action\" value = \"Search\" /><br><br>\n" +
                "    <input style=\"background-color: #C2F9BB; color: #20063B\" type = \"submit\" name=\"action\" value = \"Messages\" /><br>\n" +
                "</form>\n";
    }

    private String getHour() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date currentDate = new Date(System.currentTimeMillis());

        if (previousDate == null ||
                TimeUnit.MILLISECONDS.toSeconds(Math.abs(currentDate.getTime() - previousDate.getTime())) > 5) {
            previousDate = currentDate;

            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(previousDate);
            hour = calendar.get(Calendar.HOUR_OF_DAY);//get hour in 24h format
            if (hour + changeHour >= 24) {
                hour = 0;
                changeHour = 0;
            } else hour += changeHour++;
        }

        return sdf.format(previousDate);
    }

    private String generateSearchResultHtmlText(String cityName, String cityLatitude, String cityLongitude) {
        String result;

        if (!cityName.trim().isEmpty()) {
            List<City> cities = getMatchedCitiesByName(cityName);
            if (cities.size() == 0)
                return "<p>There is no city to match the city name you were searching for!<p>";
            result = getHtmlTextForList(cities);
        } else if (!cityLatitude.trim().isEmpty() && !cityLongitude.trim().isEmpty()) {
            List<City> cities = getMatchedCitiesByDistance(Double.parseDouble(cityLatitude), Double.parseDouble(cityLongitude));
            result = getHtmlTextForList(cities);
        } else {
            result = "<p>Something went wrong! Please, try again.</p>";
        }

        return result;
    }

    private List<City> getMatchedCitiesByDistance(Double latitude, Double longitude) {
        List<City> allCities = dataProvider.getCities();
        double minDistance = DistanceCalculator.computeDistance(allCities.get(0).getLatitude(), allCities.get(0).getLongitude(), latitude, longitude);

        for (City city : allCities) {
            if (minDistance > DistanceCalculator.computeDistance(city.getLatitude(), city.getLongitude(), latitude, longitude)) {
                minDistance = DistanceCalculator.computeDistance(city.getLatitude(), city.getLongitude(), latitude, longitude);
            }
        }

        List<City> result = new ArrayList<>();
        for (City city : allCities) {
            if (Math.abs(minDistance - DistanceCalculator.computeDistance(city.getLatitude(), city.getLongitude(), latitude, longitude)) < 1e-3) {
                result.add(city);
            }
        }

        return result;
    }

    private List<City> getMatchedCitiesByName(String cityName) {
        Set<City> matchedCities = new HashSet<>();

        List<String> possibleMatches = getAllSubstringsOfString(cityName);

        List<City> allCities = dataProvider.getCities();
        for (City city : allCities) {
            for (String match : possibleMatches)
                if (city.getName().toLowerCase().startsWith(match)) {
                    matchedCities.add(city);
                    break;
                }
        }

        return new ArrayList<>(matchedCities);
    }

    private List<String> getAllSubstringsOfString(String str) {
        List<String> possibleMatches = new ArrayList<>();
        for (int index = 0; index < str.length(); ++index) {
            possibleMatches.add(str.substring(0, index + 1).toLowerCase());
        }
        Collections.reverse(possibleMatches);

        return possibleMatches;
    }

    private String getHtmlTextForList(List<City> cities) {
        StringBuilder result = new StringBuilder("<ul style=\"list-style: none;\">\n");
        for (City city : cities) {
            result.append("<li><b style=\"color: #CC3363;\">").append(city.toString()).
                    append("</b> ").append(getListOfProperties(city, hour)).append("<br>\n");
        }
        result.append("</ul>\n");

        return result.toString();
    }

    private String getListOfProperties(City city, int hour) {
        StringBuilder result = new StringBuilder();
        for (int index = hour; index < city.getTemperatures().size(); ++index) {
            result.append("<br>").append(city.getWeatherPropertiesForHour(index));
        }
        for (int index = 0; index < hour; ++index) {
            result.append("<br>").append(city.getWeatherPropertiesForHour(index));
        }
        result.append("<br>");
        return result.toString();
    }
}
