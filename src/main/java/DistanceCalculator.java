public class DistanceCalculator {
    public static Double computeDistance(Double latitudeFrom, Double longitudeFrom, Double latitudeTo, Double longitudeTo) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(latitudeTo-latitudeFrom);
        double dLng = Math.toRadians(longitudeTo-longitudeFrom);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(latitudeFrom)) * Math.cos(Math.toRadians(latitudeTo)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c;
    }
}
