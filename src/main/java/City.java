import java.util.List;

public class City {

    private final String name;
    private final Double latitude;
    private final Double longitude;

    private final WeatherProperties weatherProperties;

    public String getName() {
        return name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public City(String cityName, Double cityLatitude, Double cityLongitude) {
        name = cityName;
        latitude = cityLatitude;
        longitude = cityLongitude;

        weatherProperties = new WeatherProperties();
    }

    @Override
    public String toString() {
        return name + " Lat: " + latitude + " Long: " + longitude;
    }

    public List<Integer> getTemperatures() {
        return weatherProperties.getTemperatures();
    }

    public void setTemperatures(List<Integer> temperatures) {
        weatherProperties.setTemperatures(temperatures);
    }

    public List<Integer> getRainfall() {
        return weatherProperties.getRainfall();
    }

    public void setRainfall(List<Integer> rainfall) {
        weatherProperties.setRainfall(rainfall);
    }

    public List<Integer> getHumidity() {
        return weatherProperties.getHumidity();
    }

    public void setHumidity(List<Integer> humidity) {
        weatherProperties.setHumidity(humidity);
    }

    public String getWeatherPropertiesForHour(int hour) {
        return "Hour: " + hour + " Degrees: " + getTemperatures().get(hour) +
                " Rainfall: " + getRainfall().get(hour) +
                "% Humidity: " + getHumidity().get(hour) + "%";
    }
}
