import java.util.ArrayList;
import java.util.List;

public class WeatherProperties {

    private List<Integer> temperatures;
    private List<Integer> rainfall;
    private List<Integer> humidity;

    public List<Integer> getRainfall() {
        return rainfall;
    }

    public void setRainfall(List<Integer> rainfall) {
        this.rainfall = rainfall;
    }

    public List<Integer> getHumidity() {
        return humidity;
    }

    public void setHumidity(List<Integer> humidity) {
        this.humidity = humidity;
    }

    public WeatherProperties() {
        temperatures = new ArrayList<>();
    }

    public List<Integer> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(List<Integer> temperatures) {
        this.temperatures = temperatures;
    }
}
